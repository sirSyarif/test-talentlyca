## Talentlyca Test

#### Application Layout

[![Layout](https://i.ibb.co/ph3pC7x/image-2023-05-31-021716053.png 'Layout')](https://i.ibb.co/ph3pC7x/image-2023-05-31-021716053.png 'Layout')

#### Output after submit

[![Output](https://i.ibb.co/mGHqjBr/image-2023-05-31-021846996.png 'Output')](https://i.ibb.co/mGHqjBr/image-2023-05-31-021846996.png 'Output')

    {
        "aspek_penilaian_1": {
            "mahasiswa_1": 3,
            "mahasiswa_2": 4,
            "mahasiswa_3": 4,
            "mahasiswa_4": 4,
            "mahasiswa_5": 1,
            "mahasiswa_6": 4,
            "mahasiswa_7": 1,
            "mahasiswa_8": 3,
            "mahasiswa_9": 1,
            "mahasiswa_10": 4
        },
        "aspek_penilaian_2": {
            "mahasiswa_1": 2,
            "mahasiswa_2": 4,
            "mahasiswa_3": 1,
            "mahasiswa_4": 1,
            "mahasiswa_5": 4,
            "mahasiswa_6": 1,
            "mahasiswa_7": 4,
            "mahasiswa_8": 1,
            "mahasiswa_9": 4,
            "mahasiswa_10": 1
        },
        "aspek_penilaian_3": {
            "mahasiswa_1": 5,
            "mahasiswa_2": 5,
            "mahasiswa_3": 5,
            "mahasiswa_4": 6,
            "mahasiswa_5": 1,
            "mahasiswa_6": 5,
            "mahasiswa_7": 1,
            "mahasiswa_8": 5,
            "mahasiswa_9": 1,
            "mahasiswa_10": 4
        },
        "aspek_penilaian_4": {
            "mahasiswa_1": 6,
            "mahasiswa_2": 1,
            "mahasiswa_3": 1,
            "mahasiswa_4": 5,
            "mahasiswa_5": 6,
            "mahasiswa_6": 1,
            "mahasiswa_7": 5,
            "mahasiswa_8": 1,
            "mahasiswa_9": 4,
            "mahasiswa_10": 1
        }
    }

#### Library List

- React redux
- @reactjs/toolkit
- tailwindcss
