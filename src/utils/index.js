export const titleAdjuster = (string) => {
  let str = string.replace(/_/g, ' ');

  str = str.replace(/\b\w/g, (match) => {
    return match.toUpperCase();
  });

  return str;
};
