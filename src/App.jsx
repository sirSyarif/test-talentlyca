import React from 'react';
import { useSelector } from 'react-redux';

// component
import List from './component/list';

// redux
import { penilaianState } from './redux/penilaian/penilaianSlice';

function App() {
  const { penilaian } = useSelector(penilaianState);
  return (
    <form
      onSubmit={(e) => {
        e.preventDefault();
        console.log(penilaian);
      }}
    >
      <div className="flex flex-col justify-center w-100 overflow-x-auto h-screen">
        <div className="bg-white w-1/2 m-auto p-10 rounded-2xl">
          <h1 className="text-center text-3xl font-bold pb-6 m-auto">
            Aplikasi Penilaian Mahasiswa
          </h1>
          <div className="overflow-x-auto">
            <table className="table-auto overflow-scroll w-full">
              <thead>
                <tr>
                  <th></th>
                  <th>Aspek Penilaian 1</th>
                  <th>Aspek Penilaian 2</th>
                  <th>Aspek Penilaian 3</th>
                  <th>Aspek Penilaian 4</th>
                </tr>
              </thead>
              <tbody>
                {[...Array(10)].map((_mhs, i) => (
                  <List key={i} mahasiswa={`mahasiswa_${i + 1}`} />
                ))}
              </tbody>
            </table>
          </div>
          <button type="submit" className="float-right my-5 mx-2 px-3 py-2 bg-black text-white ">
            Simpan
          </button>
        </div>
      </div>
    </form>
  );
}

export default App;
