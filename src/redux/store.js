import { configureStore } from '@reduxjs/toolkit';

import penilaianSlice from './penilaian/penilaianSlice';

export const store = configureStore({
  reducer: {
    penilaian: penilaianSlice
  }
});
