import { createSlice } from '@reduxjs/toolkit';

//data
import { dataPenilaian } from '../../data/masterData';

export const penilaianSlice = createSlice({
  name: 'penilaian',
  initialState: dataPenilaian,
  reducers: {
    setPenilaian: (state, action) => {
      const name = action.payload.name;
      const value = action.payload.value;
      const mahasiswa = action.payload.mahasiswa;
      state[name][mahasiswa] = Number(value, 10);
    }
  }
});

export const penilaianState = (state) => state;

export const { setPenilaian } = penilaianSlice.actions;

export default penilaianSlice.reducer;
