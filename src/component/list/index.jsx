import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// asset
import Person from '../../assets/person.png';

// utils
import { titleAdjuster } from '../../utils';

//redux
import { penilaianState, setPenilaian } from '../../redux/penilaian/penilaianSlice';

export default function List({ mahasiswa }) {
  const dispatch = useDispatch();
  const { penilaian } = useSelector(penilaianState);

  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    dispatch(setPenilaian({ name, value, mahasiswa }));
  };

  const listenChange = useCallback((e) => {
    handleChange(e);
  }, []);

  return (
    <tr>
      <td className="pr-5 pb-1">
        <div className="flex flex-row px-2 w-full whitespace-nowrap items-center">
          <img src={Person} alt="person image" width={30} height={30} className="basis-1/4 mr-2" />
          <p className="basis-3/4">{titleAdjuster(mahasiswa)}</p>
        </div>
      </td>
      {[...Array(4)].map((item, index) => (
        <td key={index} className="pb-1">
          <select
            name={`aspek_penilaian_${index + 1}`}
            value={penilaian[`aspek_penilaian_${index + 1}`][mahasiswa]}
            onChange={listenChange}
            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5  dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500"
          >
            {[...Array(10)].map((_aspek, i) => (
              <option value={i + 1} key={i}>
                {i + 1}
              </option>
            ))}
          </select>
        </td>
      ))}
    </tr>
  );
}
